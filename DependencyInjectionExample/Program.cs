﻿using DependencyInjectionExample.Infrastructure;

namespace DependencyInjectionExample
{
    public class Program
    {
        static void Main(string[] args)
        {
            var boostrapper = Bootstrap.WithWindsor();
            boostrapper.Run();
            boostrapper.Dispose();
        }
    }
}
