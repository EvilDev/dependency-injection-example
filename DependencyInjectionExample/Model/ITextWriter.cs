﻿namespace DependencyInjectionExample.Model
{
    public interface ITextWriter
    {
        void Write(string message);
    }
}