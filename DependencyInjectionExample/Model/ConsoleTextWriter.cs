﻿using System;

namespace DependencyInjectionExample.Model
{
    public class ConsoleTextWriter : ITextWriter
    {
        public void Write(string message)
        {
            Console.WriteLine(message);
        }
    }
}