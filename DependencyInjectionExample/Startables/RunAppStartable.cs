﻿using Castle.Core;
using DependencyInjectionExample.Model;

namespace DependencyInjectionExample.Startables
{
    public class RunAppStartable : IStartable
    {
        private readonly ITextWriter _writer;

        public RunAppStartable(ITextWriter writer)
        {
            _writer = writer;
        }

        public void Start()
        {
            _writer.Write("Hello World!");
        }

        public void Stop()
        {
            _writer.Write("Good-bye World!");
        }
    }
}