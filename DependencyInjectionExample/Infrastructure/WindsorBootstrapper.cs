using Castle.Facilities.Startable;
using Castle.Windsor;
using Castle.Windsor.Installer;

namespace DependencyInjectionExample.Infrastructure
{
    public class WindsorBootstrapper : IBootstrapper
    {
        private readonly IWindsorContainer _container;

        public WindsorBootstrapper(IWindsorContainer container)
        {
            _container = container;
        }

        public void Dispose()
        {
            _container.Dispose();
        }

        public void Run()
        {
            _container.AddFacility<StartableFacility>();

            _container.Install(FromAssembly.This());
        }
    }
}