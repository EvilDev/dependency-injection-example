﻿using Castle.Windsor;

namespace DependencyInjectionExample.Infrastructure
{
    public static class Bootstrap
    {
         public static IBootstrapper WithWindsor()
         {
             var container = new WindsorContainer();
             return new WindsorBootstrapper(container);
         }
    }
}