﻿using System;

namespace DependencyInjectionExample.Infrastructure
{
    public interface IBootstrapper : IDisposable
    {
        void Run();
    }
}